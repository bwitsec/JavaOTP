# JavaOTP

Java OTP Generator for HOTP (RFC 4226) and TOTP (RFC 6238)

This software provides classes and a command line interface to produce counter-based (HOTP)
or time-based (TOTP) one-time-passwords.

HOTP and TOTP are slightly modified versions of the original code
published in the respective RFCs.

- [HOTP: An HMAC-Based One-Time Password Algorithm](https://tools.ietf.org/html/rfc4226)
- [TOTP: Time-Based One-Time Password Algorithm](https://tools.ietf.org/html/rfc6238)

Also uses a stripped down version of java-getopt to parse command line arguments.

- [java-getopt](https://github.com/arenn/java-getopt)

JavaOTP draws inspiration from [libotp](https://github.com/kamranzafar/libotp) but reduces complexity (and with it flexibility) and adds a convenient command line interface for immediate usage.

For alternatives see:

- [java-getopt](https://github.com/arenn/java-getopt)
- [aerogear-otp-java](https://github.com/aerogear/aerogear-otp-java)
- [jotp](https://www.cs.umd.edu/~harry/jotp/)

## License Information

Each file has individual licensens. See source files. Licenses used are FreeBSD, LGPL, MIT.

## Build and Clean

Compiles classes and creates executable jar

        # ant
        
Just build classes

        # ant build

Clean up everything

        # ant distclean


## Usage

After build, executable jar will be in dist. Change to that directory.

Get help

        # java -jar JavaOTP -h
        usage: java javaotp.OTP [Options]
        Generate a one time password using a specific ALGORITHM with DIGITS number of digits
        
        -a: 	 Algorithm, hotp or totp. Default hotp.
        -d: 	 Number of digits for otp, 1-8. Default 6.
        -k: 	 Secret Key in HEX (without 0x prefix). Default 3132333435363738393031323334353637383930.
        -s: 	 Add checksum to otp result. Default clear
        
        HOTP Options
        -c: 	 Moving Factor. Default 0.
        -o: 	 Trucation offset. Default -1.
        
        TOTP Options
        -t: 	 Time in seconds since unix epoch. Default current time (date +%s).
        -f: 	 Hash function to use. SHA1 | SHA256 | SHA512. Default SHA1.
        -w: 	 Time window in seconds. Default 30.



Run with default values

        # java -jar JavaOTP
        
Generate TOTP with 8 digits, using current time and custom key

        # java -jar JavaOTP -a totp -d 8 -t `date +%s` -k 20d1aa2a94be897d85690b72c66291c9e3cdeec5
        
Generate HOTP with 8 digits, with checksum, moving factor 10, and custom key

        # java -jar JavaOTP -a hotp -d 8 -c 10 -s -k 20d1aa2a94be897d85690b72c66291c9e3cdeec5
        
Run the example from RFC 4226 (page 31)

        # for i in {0..9}; do java -jar JavaOTP -c $i; done;