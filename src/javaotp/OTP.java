/*
 * OTP.java
 * 
 */

/**
 *  Copyright (C) 2016 Thomas Zink (tz < at > uni < dot > kn)
 *  
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense, 
 *  and/or sell copies of the Software, and to permit persons to whom the 
 *  Software is furnished to do so, subject to the following conditions:
 *  
 *  The above copyright notice and this permission notice shall be included in 
 *  all copies or substantial portions of the Software.
 *  
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 */
package javaotp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Properties;

public class OTP {
	private OTP() { }

	static final String PROP_ALG = "algorithm";
	static final String PROP_KEY = "key";
	static final String PROP_CODEDIGITS = "codeDigits";
	static final String PROP_ADDCHECKSUM = "addChecksum";

	private static final void usage() {
		String use = "usage: java -jar JavaOTP [Options]\n"
			+ "Generate a one time password using a specific ALGORITHM with DIGITS number of digits\n" + "\n"
			+ "Common Options:\n"
			+ "-a: \t Algorithm, hotp or totp. Default hotp.\n"
			+ "-d: \t Number of digits for otp, 1-8. Default 6.\n"
			+ "-k: \t Secret Key in HEX (without 0x prefix). Default 3132333435363738393031323334353637383930.\n"
			+ "-s: \t Add checksum to otp result. Default clear\n"
			+ "\n" + "HOTP Options\n"
			+ "-c: \t Moving Factor. Default 0.\n"
			+ "-o: \t Trucation offset. Default -1.\n"
			+ "\n" + "TOTP Options\n"
			+ "-t: \t Time in seconds since unix epoch. Default current time (date +%s).\n"
			+ "-f: \t Hash function to use. SHA1 | SHA256 | SHA512. Default SHA1.\n"
			+ "-w: \t Time window in seconds. Default 30.\n"
		;
		System.out.println(use);
		System.exit(0);
	}
	
	private static final Properties checkProperties(String[] args) {
		Properties props = new Properties();
		props.setProperty(PROP_ALG, "hotp");
		props.setProperty(PROP_KEY, "3132333435363738393031323334353637383930");
		props.setProperty(PROP_CODEDIGITS, "6");
		props.setProperty(PROP_ADDCHECKSUM, "false");
		props.setProperty(HOTP.PROP_MOVINGFACTOR, "0");
		props.setProperty(HOTP.PROP_TRUNCATIONOFFSET, "-1");
		props.setProperty(TOTP.PROP_TIME, String.valueOf(new Date().getTime() / 1000));
		props.setProperty(TOTP.PROP_WINDOW, "30");
		props.setProperty(TOTP.PROP_CRYPTO, "HmacSHA1");
		
		Getopt opts = new Getopt("OTP", args, "?ha:d:k:c:o:st:f:w:");
		
		int opt;
		while ((opt = opts.getopt()) != -1) {
			switch (opt) {
			case 'a':
				String algo = opts.getOptarg().toLowerCase();
				if (! algo.equals("hotp") && ! algo.equals("totp")) {
					System.out.println("Illegal Algorithm " + algo + ", using default.");
				} else {
					props.setProperty(PROP_ALG, opts.getOptarg());
				}
				break;
			case 'd':
				int digits = Integer.parseInt(opts.getOptarg());
				if ((0 > digits) || (digits > 8)) {
					System.out.println("Digits " + digits + " out of range, using default.");
				} else {
					props.setProperty(PROP_CODEDIGITS, opts.getOptarg());
				}
				break;
			case 'k':
				props.setProperty(PROP_KEY, opts.getOptarg());
				break;
			case 'c':
				props.setProperty(HOTP.PROP_MOVINGFACTOR, opts.getOptarg());
				break;
			case 'o':
				props.setProperty(HOTP.PROP_TRUNCATIONOFFSET, opts.getOptarg());
				break;
			case 's':
				props.setProperty(PROP_ADDCHECKSUM, "true");
				break;
			case 't':
				props.setProperty(TOTP.PROP_TIME, opts.getOptarg());
				break;
			case 'f':
				String crypto = opts.getOptarg().toUpperCase();
				if (!crypto.equals("SHA1") && !crypto.equals("SHA256") && !crypto.equals("SHA512")) {
					System.out.println("Unknown hash function " + crypto + ", using default.");
				} else {
					props.setProperty(TOTP.PROP_CRYPTO, "Hmac" + crypto);
				}
				break;
			case 'w':
				props.setProperty(TOTP.PROP_WINDOW, opts.getOptarg());
				break;
			case '?':
			case 'h':
			default:
				usage();
				break;
			}
		}
		
		return props;
	}
	
	public static String generateOTP(String algo, Properties props) throws InvalidKeyException, NoSuchAlgorithmException {
		String result = null;

		if (algo.equals("hotp")) {
			result = HOTP.generateOTP(props);
		}
		else if (algo.equals("totp")) {
			result = TOTP.generateOTP(props);
		}
		
		return result;
	}

	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException {
		Properties props = checkProperties(args);
		String otp = generateOTP(props.getProperty(PROP_ALG), props);
		System.out.println(otp);
	}
}
