package javaotp;
/**************************************************************************
/* Getopt.java -- Java port of GNU getopt from glibc 2.0.6
/*
/* Copyright (c) 1987-1997 Free Software Foundation, Inc.
/* Java Port Copyright (c) 1998 by Aaron M. Renn (arenn@urbanophile.com)
/*
/* This program is free software; you can redistribute it and/or modify
/* it under the terms of the GNU Library General Public License as published 
/* by  the Free Software Foundation; either version 2 of the License or
/* (at your option) any later version.
/*
/* This program is distributed in the hope that it will be useful, but
/* WITHOUT ANY WARRANTY; without even the implied warranty of
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
/* GNU Library General Public License for more details.
/*
/* You should have received a copy of the GNU Library General Public License
/* along with this program; see the file COPYING.LIB.  If not, write to 
/* the Free Software Foundation Inc., 59 Temple Place - Suite 330, 
/* Boston, MA  02111-1307 USA
/**************************************************************************/

import java.text.MessageFormat;

/**
 * This is a Java port of GNU getopt, a class for parsing command line arguments
 * passed to programs. It it based on the C getopt() functions in glibc 2.0.6
 * and should parse options in a 100% compatible manner. If it does not, that is
 * a bug.
 * <p>
 * Please send all bug reports, requests, and comments to
 * <a href="mailto:arenn@urbanophile.com">arenn@urbanophile.com</a>.
 *
 * Original @version 1.0.7
 *
 * @author Roland McGrath (roland@gnu.ai.mit.edu)
 * @author Ulrich Drepper (drepper@cygnus.com)
 * @author Aaron M. Renn (arenn@urbanophile.com)
 * 
 * @author Thomas Zink (tz@uni.kn)
 * Version: 2016030201 (tz)
 * Removed all LongOpt related methods and fields
 * Removed localization related methods, fields, imports, resources
 * Removed some not crucial methods (setOptstring,getOptind,setOptind,setArgv,setOpterr,getOptopt)
 */
public class Getopt extends Object {

	/**
	 * Describe how to deal with options that follow non-option ARGV-elements.
	 *
	 * If the caller did not specify anything, the default is REQUIRE_ORDER if
	 * the property gnu.posixly_correct is defined, PERMUTE otherwise.
	 *
	 * The special argument `--' forces an end of option-scanning regardless of
	 * the value of `ordering'. In the case of RETURN_IN_ORDER, only `--' can
	 * cause `getopt' to return -1 with `optind' != ARGC.
	 *
	 * REQUIRE_ORDER means don't recognize them as options; stop option
	 * processing when the first non-option is seen. This is what Unix does.
	 * This mode of operation is selected by either setting the property
	 * gnu.posixly_correct, or using `+' as the first character of the list of
	 * option characters.
	 */
	protected static final int REQUIRE_ORDER = 1;

	/**
	 * PERMUTE is the default. We permute the contents of ARGV as we scan, so
	 * that eventually all the non-options are at the end. This allows options
	 * to be given in any order, even with programs that were not written to
	 * expect this.
	 */
	protected static final int PERMUTE = 2;

	/**
	 * RETURN_IN_ORDER is an option available to programs that were written to
	 * expect options and other ARGV-elements in any order and that care about
	 * the ordering of the two. We describe each non-option ARGV-element as if
	 * it were the argument of an option with character code 1. Using `-' as the
	 * first character of the list of option characters selects this mode of
	 * operation.
	 */
	protected static final int RETURN_IN_ORDER = 3;

	/**
	 * For communication from `getopt' to the caller. When `getopt' finds an
	 * option that takes an argument, the argument value is returned here. Also,
	 * when `ordering' is RETURN_IN_ORDER, each non-option ARGV-element is
	 * returned here.
	 */
	protected String optarg;

	/**
	 * Index in ARGV of the next element to be scanned. This is used for
	 * communication to and from the caller and for communication between
	 * successive calls to `getopt'.
	 *
	 * On entry to `getopt', zero means this is the first call; initialize.
	 *
	 * When `getopt' returns -1, this is the index of the first of the
	 * non-option elements that the caller should itself scan.
	 *
	 * Otherwise, `optind' communicates from one call to the next how much of
	 * ARGV has been scanned so far.
	 */
	protected int optind = 0;

	/**
	 * Callers store false here to inhibit the error message for unrecognized
	 * options.
	 */
	protected boolean opterr = true;

	/**
	 * When an unrecognized option is encountered, getopt will return a '?' and
	 * store the value of the invalid option here.
	 */
	protected int optopt = '?';

	/**
	 * The next char to be scanned in the option-element in which the last
	 * option character we returned was found. This allows us to pick up the
	 * scan where we left off.
	 *
	 * If this is zero, or a null string, it means resume the scan by advancing
	 * to the next ARGV-element.
	 */
	protected String nextchar;

	/**
	 * This is the string describing the valid short options.
	 */
	protected String optstring;

	/**
	 * The flag determines whether or not we operate in strict POSIX compliance
	 */
	protected boolean posixly_correct;

	/**
	 * The index of the first non-option in argv[]
	 */
	protected int first_nonopt = 1;

	/**
	 * The index of the last non-option in argv[]
	 */
	protected int last_nonopt = 1;

	/**
	 * Flag to tell getopt to immediately return -1 the next time it is called.
	 */
	private boolean endparse = false;

	/**
	 * Saved argument list passed to the program
	 */
	protected String[] argv;

	/**
	 * Determines whether we permute arguments or not
	 */
	protected int ordering;

	/**
	 * Name to print as the program name in error messages. This is necessary
	 * since Java does not place the program name in argv[0]
	 */
	protected String progname;

	/**
	 * Construct a Getopt instance with given input data that is capable of
	 * parsing long options and short options. Contrary to what you might think,
	 * the flag 'long_only' does not determine whether or not we scan for only
	 * long arguments. Instead, a value of true here allows long arguments to
	 * start with a '-' instead of '--' unless there is a conflict with a short
	 * option name.
	 *
	 * @param progname
	 *            The name to display as the program name when printing errors
	 * @param argv
	 *            The String array passed as the command ilne to the program
	 * @param optstring
	 *            A String containing a description of the valid short args for
	 *            this program
	 * @param long_options
	 *            An array of LongOpt objects that describes the valid long args
	 *            for this program
	 * @param long_only
	 *            true if long options that do not conflict with short options
	 *            can start with a '-' as well as '--'
	 */
	public Getopt(String progname, String[] argv, String optstring) {
		if (optstring.length() == 0)
			optstring = " ";

		// This function is essentially _getopt_initialize from GNU getopt
		this.progname = progname;
		this.argv = argv;
		this.optstring = optstring;

		// Check for property "gnu.posixly_correct" to determine whether to
		// strictly follow the POSIX standard. This replaces the
		// "POSIXLY_CORRECT"
		// environment variable in the C version
		if (System.getProperty("gnu.posixly_correct", null) == null)
			posixly_correct = false;
		else {
			posixly_correct = true;
		}

		// Determine how to handle the ordering of options and non-options
		if (optstring.charAt(0) == '-') {
			ordering = RETURN_IN_ORDER;
			if (optstring.length() > 1)
				this.optstring = optstring.substring(1);
		} else if (optstring.charAt(0) == '+') {
			ordering = REQUIRE_ORDER;
			if (optstring.length() > 1)
				this.optstring = optstring.substring(1);
		} else if (posixly_correct) {
			ordering = REQUIRE_ORDER;
		} else {
			ordering = PERMUTE; // The normal default case
		}
	}

	/**
	 * For communication from `getopt' to the caller. When `getopt' finds an
	 * option that takes an argument, the argument value is returned here. Also,
	 * when `ordering' is RETURN_IN_ORDER, each non-option ARGV-element is
	 * returned here. No set method is provided because setting this variable
	 * has no effect.
	 */
	public String getOptarg() {
		return (optarg);
	}

	/**
	 * Exchange the shorter segment with the far end of the longer segment. That
	 * puts the shorter segment into the right place. It leaves the longer
	 * segment in the right place overall, but it consists of two parts that
	 * need to be swapped next. This method is used by getopt() for argument
	 * permutation.
	 */
	protected void exchange(String[] argv) {
		int bottom = first_nonopt;
		int middle = last_nonopt;
		int top = optind;
		String tem;

		while (top > middle && middle > bottom) {
			if (top - middle > middle - bottom) {
				// Bottom segment is the short one.
				int len = middle - bottom;
				int i;

				// Swap it with the top part of the top segment.
				for (i = 0; i < len; i++) {
					tem = argv[bottom + i];
					argv[bottom + i] = argv[top - (middle - bottom) + i];
					argv[top - (middle - bottom) + i] = tem;
				}
				// Exclude the moved bottom segment from further swapping.
				top -= len;
			} else {
				// Top segment is the short one.
				int len = top - middle;
				int i;

				// Swap it with the bottom part of the bottom segment.
				for (i = 0; i < len; i++) {
					tem = argv[bottom + i];
					argv[bottom + i] = argv[middle + i];
					argv[middle + i] = tem;
				}
				// Exclude the moved top segment from further swapping.
				bottom += len;
			}
		}

		// Update records for the slots the non-options now occupy.
		first_nonopt += (optind - last_nonopt);
		last_nonopt = optind;
	}

	/**************************************************************************/

	/**
	 * This method returns a char that is the current option that has been
	 * parsed from the command line. If the option takes an argument, then the
	 * internal variable 'optarg' is set which is a String representing the the
	 * value of the argument. This value can be retrieved by the caller using
	 * the getOptarg() method. If an invalid option is found, an error message
	 * is printed and a '?' is returned. The name of the invalid option
	 * character can be retrieved by calling the getOptopt() method. When there
	 * are no more options to be scanned, this method returns -1. The index of
	 * first non-option element in argv can be retrieved with the getOptind()
	 * method.
	 *
	 * @return Various things as described above
	 */
	public int getopt() {
		optarg = null;

		if (endparse == true)
			return (-1);

		if ((nextchar == null) || (nextchar.equals(""))) {
			// If we have just processed some options following some
			// non-options,
			// exchange them so that the options come first.
			if (last_nonopt > optind)
				last_nonopt = optind;
			if (first_nonopt > optind)
				first_nonopt = optind;

			if (ordering == PERMUTE) {
				// If we have just processed some options following some
				// non-options,
				// exchange them so that the options come first.
				if ((first_nonopt != last_nonopt) && (last_nonopt != optind))
					exchange(argv);
				else if (last_nonopt != optind)
					first_nonopt = optind;

				// Skip any additional non-options
				// and extend the range of non-options previously skipped.
				while ((optind < argv.length)
						&& (argv[optind].equals("") || (argv[optind].charAt(0) != '-') || argv[optind].equals("-"))) {
					optind++;
				}

				last_nonopt = optind;
			}

			// The special ARGV-element `--' means premature end of options.
			// Skip it like a null option,
			// then exchange with previous non-options as if it were an option,
			// then skip everything else like a non-option.
			if ((optind != argv.length) && argv[optind].equals("--")) {
				optind++;

				if ((first_nonopt != last_nonopt) && (last_nonopt != optind))
					exchange(argv);
				else if (first_nonopt == last_nonopt)
					first_nonopt = optind;

				last_nonopt = argv.length;

				optind = argv.length;
			}

			// If we have done all the ARGV-elements, stop the scan
			// and back over any non-options that we skipped and permuted.
			if (optind == argv.length) {
				// Set the next-arg-index to point at the non-options
				// that we previously skipped, so the caller will digest them.
				if (first_nonopt != last_nonopt)
					optind = first_nonopt;

				return (-1);
			}

			// If we have come to a non-option and did not permute it,
			// either stop the scan or describe it to the caller and pass it by.
			if (argv[optind].equals("") || (argv[optind].charAt(0) != '-') || argv[optind].equals("-")) {
				if (ordering == REQUIRE_ORDER)
					return (-1);

				optarg = argv[optind++];
				return (1);
			}

			// We have found another option-ARGV-element.
			// Skip the initial punctuation.
			if (argv[optind].startsWith("--"))
				nextchar = argv[optind].substring(2);
			else
				nextchar = argv[optind].substring(1);
		}

		// Decode the current option-ARGV-element.

		// Look at and handle the next short option-character */
		int c = nextchar.charAt(0); // **** Do we need to check for empty str?
		if (nextchar.length() > 1)
			nextchar = nextchar.substring(1);
		else
			nextchar = "";

		String temp = null;
		if (optstring.indexOf(c) != -1)
			temp = optstring.substring(optstring.indexOf(c));

		if (nextchar.equals(""))
			++optind;

		if ((temp == null) || (c == ':')) {
			if (opterr) {
				if (posixly_correct) {
					// 1003.2 specifies the format of this message
					Object[] msgArgs = { progname, new Character((char) c).toString() };
					System.err.println(MessageFormat.format("illegal option -- {1}", msgArgs));
				} else {
					Object[] msgArgs = { progname, new Character((char) c).toString() };
					System.err.println(MessageFormat.format("invalid option -- {1}", msgArgs));
				}
			}

			optopt = c;

			return ('?');
		}

		// Convenience. Treat POSIX -W foo same as long option --foo
		if ((temp.charAt(0) == 'W') && (temp.length() > 1) && (temp.charAt(1) == ';')) {
			if (!nextchar.equals("")) {
				optarg = nextchar;
			}
			// No further cars in this argv element and no more argv elements
			else if (optind == argv.length) {
				if (opterr) {
					// 1003.2 specifies the format of this message.
					Object[] msgArgs = { progname, new Character((char) c).toString() };
					System.err.println(MessageFormat.format("option requires an argument -- {1}", msgArgs));
				}

				optopt = c;
				if (optstring.charAt(0) == ':')
					return (':');
				else
					return ('?');
			} else {
				// We already incremented `optind' once;
				// increment it again when taking next ARGV-elt as argument.
				nextchar = argv[optind];
				optarg = argv[optind];
			}
		}

		if ((temp.length() > 1) && (temp.charAt(1) == ':')) {
			if ((temp.length() > 2) && (temp.charAt(2) == ':'))
			// This is an option that accepts and argument optionally
			{
				if (!nextchar.equals("")) {
					optarg = nextchar;
					++optind;
				} else {
					optarg = null;
				}

				nextchar = null;
			} else {
				if (!nextchar.equals("")) {
					optarg = nextchar;
					++optind;
				} else if (optind == argv.length) {
					if (opterr) {
						// 1003.2 specifies the format of this message
						Object[] msgArgs = { progname, new Character((char) c).toString() };
						System.err.println(MessageFormat.format("option requires an argument -- {1}", msgArgs));
					}

					optopt = c;

					if (optstring.charAt(0) == ':')
						return (':');
					else
						return ('?');
				} else {
					optarg = argv[optind];
					++optind;

					// Ok, here's an obscure Posix case. If we have o:, and
					// we get -o -- foo, then we're supposed to skip the --,
					// end parsing of options, and make foo an operand to -o.
					// Only do this in Posix mode.
					if ((posixly_correct) && optarg.equals("--")) {
						// If end of argv, error out
						if (optind == argv.length) {
							if (opterr) {
								// 1003.2 specifies the format of this message
								Object[] msgArgs = { progname, new Character((char) c).toString() };
								System.err.println(MessageFormat.format("option requires an argument -- {1}", msgArgs));
							}

							optopt = c;

							if (optstring.charAt(0) == ':')
								return (':');
							else
								return ('?');
						}

						// Set new optarg and set to end
						// Don't permute as we do on -- up above since we
						// know we aren't in permute mode because of Posix.
						optarg = argv[optind];
						++optind;
						first_nonopt = optind;
						last_nonopt = argv.length;
						endparse = true;
					}
				}
				nextchar = null;
			}
		}

		return (c);
	}

} // Class Getopt
