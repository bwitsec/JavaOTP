package javaotp;

import java.lang.reflect.UndeclaredThrowableException;
import java.math.BigInteger;
import java.security.GeneralSecurityException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Common {
	private Common() {}

	/* Copyright (C) 2004, OATH.  All rights reserved.
	*
	* License to copy and use this software is granted provided that it
	* is identified as the "OATH HOTP Algorithm" in all material
	* mentioning or referencing this software or this function.
	*
	* License is also granted to make and use derivative works provided
	* that such works are identified as
	*  "derived from OATH HOTP algorithm"
	* in all material mentioning or referencing the derived work.
	*
	* OATH (Open AuTHentication) and its members make no
	* representations concerning either the merchantability of this
	* software or the suitability of this software for any particular
	* purpose.
	*
	* It is provided "as is" without express or implied warranty
	* of any kind and OATH AND ITS MEMBERS EXPRESSLY DISCLAIMS
	* ANY WARRANTY OR LIABILITY OF ANY KIND relating to this software.
	*
	* These notices must be retained in any copies of any part of this
	* documentation and/or software.
	*/
	public static final int[] DIGITS_POWER =
		{1,10,100,1000,10000,100000,1000000,10000000,100000000};
	
	//These are used to calculate the check-sum digits.
	private static final int[] doubleDigits = {0,2,4,6,8,1,3,5,7,9};

	/**
		* Calculates the checksum using the credit card algorithm.
		* This algorithm has the advantage that it detects any single
		* mistyped digit and any single transposition of
		* adjacent digits.
		*
		* @param num the number to calculate the checksum for
		* @param digits number of significant places in the number
		*
		* @return the checksum of num
	*/
	public static int calcChecksum(long num, int digits) {
	 	boolean doubleDigit = true;
		int     total = 0;
		while (0 < digits--) {
			int digit = (int) (num % 10);
			num /= 10;
			if (doubleDigit) {
				digit = doubleDigits[digit];
			}
			total += digit;
			doubleDigit = !doubleDigit;
		}
		int result = total % 10;
		if (result > 0) {
			result = 10 - result;
		}
		return result;
	}
	
	/*
	Copyright (c) 2011 IETF Trust and the persons identified as
	authors of the code. All rights reserved.

	Redistribution and use in source and binary forms, with or without
	modification, is permitted pursuant to, and subject to the license
	terms contained in, the Simplified BSD License set forth in Section
	4.c of the IETF Trust's Legal Provisions Relating to IETF Documents
	(http://trustee.ietf.org/license-info).
	*/
	/**
	 * This method converts a HEX string to Byte[]
	 *
	 * @param hex:
	 *            the HEX string
	 *
	 * @return: a byte array
	 */
	public static byte[] hexStr2Bytes(String hex) {
		// Adding one byte to get the right conversion
		// Values starting with "0" can be converted
		byte[] bArray = new BigInteger("10" + hex, 16).toByteArray();

		// Copy all the REAL bytes, not the "first"
		byte[] ret = new byte[bArray.length - 1];
		for (int i = 0; i < ret.length; i++) {
			ret[i] = bArray[i + 1];
		}
		return ret;
	}
	
	/**
	 * This method uses the JCE to provide the crypto algorithm. HMAC computes a
	 * Hashed Message Authentication Code with the crypto hash algorithm as a
	 * parameter.
	 *
	 * @param crypto:
	 *            the crypto algorithm (HmacSHA1, HmacSHA256, HmacSHA512)
	 * @param keyBytes:
	 *            the bytes to use for the HMAC key
	 * @param text:
	 *            the message or text to be authenticated
	 */
	public static byte[] hmac_sha(String crypto, byte[] keyBytes, byte[] text) {
		try {
			Mac hmac;
			hmac = Mac.getInstance(crypto);
			SecretKeySpec macKey = new SecretKeySpec(keyBytes, "RAW");
			hmac.init(macKey);
			return hmac.doFinal(text);
		} catch (GeneralSecurityException gse) {
			throw new UndeclaredThrowableException(gse);
		}
	}

}
