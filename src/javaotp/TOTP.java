/*
* TOTP.java
* RFC 6238
* TOTP: Time-Based One-Time Password Algorithm
*/

/**
Copyright (c) 2011 IETF Trust and the persons identified as
authors of the code. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, is permitted pursuant to, and subject to the license
terms contained in, the Simplified BSD License set forth in Section
4.c of the IETF Trust's Legal Provisions Relating to IETF Documents
(http://trustee.ietf.org/license-info).
*/

package javaotp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.TimeZone;

/**
 * This is an example implementation of the OATH TOTP algorithm. Visit
 * www.openauthentication.org for more information.
 *
 * Original @author Johan Rydell, PortWise, Inc.
 * 
 * Modifications for javaOTP tz@uni.kn
 * 
 * Version: 2016030201 (tz)
 * Unified interface for HOTP,TOTP,OTP
 * Moved common methods and fields to class Common
 * Added Algorithm specific PROPERTY Strings
 * 
 */
public class TOTP {

	private TOTP() {}
	
	static final String PROP_TIME = "time";
	static final String PROP_CRYPTO = "crypto";
	static final String PROP_WINDOW = "window";
	
	static public String generateOTP(Properties props) throws InvalidKeyException, NoSuchAlgorithmException {
		String key = props.getProperty(OTP.PROP_KEY);
		int codeDigits = Integer.parseInt(props.getProperty(OTP.PROP_CODEDIGITS));
		boolean addChecksum =  Boolean.parseBoolean(props.getProperty(OTP.PROP_ADDCHECKSUM));
		long time = Long.parseLong(props.getProperty(PROP_TIME));
		long window = Long.parseLong(props.getProperty(PROP_WINDOW));
		String crypto = props.getProperty(PROP_CRYPTO);
		
		return generateOTP(key, codeDigits, addChecksum, time / window, crypto);
	}
	
	/**
	 * This method generates a TOTP value for the given set of parameters.
	 *
	 * @param key: 	the shared secret, HEX encoded
	 * @param time: a value that reflects a time
	 * @param codeDigits: number of digits to return
	 * @param crypto: the crypto function to use
	 *
	 * @return: a numeric String in base 10 that includes
	 *          {@link truncationDigits} digits
	 */
	public static String generateOTP(
			String key,
			int codeDigits,
			boolean addChecksum,
			long time,
			String crypto) {
		String result = null;
		int digits = addChecksum ? (codeDigits + 1) : codeDigits;
		String steps = "0";
		steps = Long.toHexString(time).toUpperCase();
		while (steps.length() < 16) {
			steps = "0" + steps;
		}
		
		byte[] msg = Common.hexStr2Bytes(steps);
		byte[] k = Common.hexStr2Bytes(key);
		byte[] hash = Common.hmac_sha(crypto, k, msg);

		// put selected bytes into result int
		int offset = hash[hash.length - 1] & 0xf;

		int binary = ((hash[offset] & 0x7f) << 24) | ((hash[offset + 1] & 0xff) << 16)
				| ((hash[offset + 2] & 0xff) << 8) | (hash[offset + 3] & 0xff);

		int otp = binary % Common.DIGITS_POWER[codeDigits];
		if (addChecksum) {
			otp =  (otp * 10) + Common.calcChecksum(otp, codeDigits);
		}

		result = Integer.toString(otp);
		while (result.length() < digits) {
			result = "0" + result;
		}
		return result;
	}

	/*
	 * test method
	 */
	public static void main(String[] args) {
		// Seed for HMAC-SHA1 - 20 bytes
		String seed = "3132333435363738393031323334353637383930";
		// Seed for HMAC-SHA256 - 32 bytes
		String seed32 = "3132333435363738393031323334353637383930" + "313233343536373839303132";
		// Seed for HMAC-SHA512 - 64 bytes
		String seed64 = "3132333435363738393031323334353637383930" + "3132333435363738393031323334353637383930"
				+ "3132333435363738393031323334353637383930" + "31323334";
		long T0 = 0;
		long X = 30;
		long testTime[] = { 59L, 1111111109L, 1111111111L, 1234567890L, 2000000000L, 20000000000L };

		String steps = "0";
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("UTC"));

		try {
			System.out.println("+---------------+-----------------------+" + "------------------+--------+--------+");
			System.out.println("|  Time(sec)    |   Time (UTC format)   " + "| Value of T(Hex)  |  TOTP  | Mode   |");
			System.out.println("+---------------+-----------------------+" + "------------------+--------+--------+");

			for (int i = 0; i < testTime.length; i++) {
				long T = (testTime[i] - T0) / X;
				steps = Long.toHexString(T).toUpperCase();
				while (steps.length() < 16)
					steps = "0" + steps;
				String fmtTime = String.format("%1$-11s", testTime[i]);
				String utcTime = df.format(new Date(testTime[i] * 1000));
				System.out.print("|  " + fmtTime + "  |  " + utcTime + "  | " + steps + " |");
				System.out.println(generateOTP(seed, 8, false, T, "HmacSHA1") + "| SHA1   |");
				System.out.print("|  " + fmtTime + "  |  " + utcTime + "  | " + steps + " |");
				System.out.println(generateOTP(seed32, 8, false, T, "HmacSHA256") + "| SHA256 |");
				System.out.print("|  " + fmtTime + "  |  " + utcTime + "  | " + steps + " |");
				System.out.println(generateOTP(seed64, 8, false, T, "HmacSHA512") + "| SHA512 |");

				System.out.println("+---------------+-----------------------+" + "------------------+--------+--------+");
			}
		} catch (final Exception e) {
			System.out.println("Error : " + e);
		}		
	}
}