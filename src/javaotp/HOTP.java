/*
* HOTP.java
* derived from OATH HOTP algorithm
* Original version: OneTimePasswordAlgorithm.java
* RFC 4226
* HOTP: An HMAC-Based One-Time Password Algorithm
*/

/* Copyright (C) 2004, OATH.  All rights reserved.
*
* License to copy and use this software is granted provided that it
* is identified as the "OATH HOTP Algorithm" in all material
* mentioning or referencing this software or this function.
*
* License is also granted to make and use derivative works provided
* that such works are identified as
*  "derived from OATH HOTP algorithm"
* in all material mentioning or referencing the derived work.
*
* OATH (Open AuTHentication) and its members make no
* representations concerning either the merchantability of this
* software or the suitability of this software for any particular
* purpose.
*
* It is provided "as is" without express or implied warranty
* of any kind and OATH AND ITS MEMBERS EXPRESSLY DISCLAIMS
* ANY WARRANTY OR LIABILITY OF ANY KIND relating to this software.
*
* These notices must be retained in any copies of any part of this
* documentation and/or software.
*/

package javaotp;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;

/**
* This class contains static methods that are used to calculate the
* One-Time Password (OTP) using JCE to provide the HMAC-SHA-1.
*
* Original @author Loren Hart
* Original @version 1.0
* 
 * Version: 2016030201 (tz)
 * Unified interface for HOTP,TOTP,OTP
 * Moved common methods and fields to class Common
 * Added Algorithm specific PROPERTY Strings
*/
public class HOTP {

	private HOTP() {}
	
	static final String PROP_MOVINGFACTOR = "movingFactor";
	static final String PROP_TRUNCATIONOFFSET = "truncationOffset";
	
	static public String generateOTP(Properties props) throws InvalidKeyException, NoSuchAlgorithmException {
		String key = props.getProperty(OTP.PROP_KEY);
		int codeDigits = Integer.parseInt(props.getProperty(OTP.PROP_CODEDIGITS));
		boolean addChecksum =  Boolean.parseBoolean(props.getProperty(OTP.PROP_ADDCHECKSUM));
		int movingFactor = Integer.parseInt(props.getProperty(PROP_MOVINGFACTOR));
		int truncationOffset = Integer.parseInt(props.getProperty(PROP_TRUNCATIONOFFSET));
		
		return generateOTP(key, codeDigits, addChecksum, movingFactor, truncationOffset);
	}
	
	/**
		* This method generates an OTP value for the given
		* set of parameters.
		*
		* @param key: the shared secret, HEX encoded
		* @param movingFactor the counter, time, or other value that
		*                     changes on a per use basis.
		* @param codeDigits   the number of digits in the OTP, not
		*                     including the checksum, if any.
		* @param addChecksum  a flag that indicates if a checksum digit
		*                     should be appended to the OTP.
		* @param truncationOffset the offset into the MAC result to
		*                     begin truncation.  If this value is out of
		*                     the range of 0 ... 15, then dynamic
		*                     truncation  will be used.
		*                     Dynamic truncation is when the last 4
		*                     bits of the last byte of the MAC are
		*                     used to determine the start offset.
		* @throws NoSuchAlgorithmException if no provider makes
		*                     either HmacSHA1 or HMAC-SHA-1
		*                     digest algorithms available.
		* @throws InvalidKeyException
		*                     The secret provided was not
		*                     a valid HMAC-SHA-1 key.
		*
		* @return A numeric String in base 10 that includes
		* {@link codeDigits} digits plus the optional checksum
		* digit if requested.
	*/
	static public String generateOTP(
		String key,
		int codeDigits,
		boolean addChecksum,
		int movingFactor,
		int truncationOffset)
		throws NoSuchAlgorithmException, InvalidKeyException
	{
		// put movingFactor value into text byte array
		String result = null;
		int digits = addChecksum ? (codeDigits + 1) : codeDigits;
		byte[] text = new byte[8];
		for (int i = text.length - 1; i >= 0; i--) {
			text[i] = (byte) (movingFactor & 0xff);
			movingFactor >>= 8;
		}
	
		byte[] secret = Common.hexStr2Bytes(key);
		byte[] hash = Common.hmac_sha("HmacSHA1",secret, text);
	
		// put selected bytes into result int
		int offset = hash[hash.length - 1] & 0xf;
		if ( (0<=truncationOffset) && (truncationOffset<(hash.length-4)) ) {
			offset = truncationOffset;
		}
		int binary =
			((hash[offset] & 0x7f) << 24)
			| ((hash[offset + 1] & 0xff) << 16)
			| ((hash[offset + 2] & 0xff) << 8)
			| (hash[offset + 3] & 0xff);
	
		int otp = binary % Common.DIGITS_POWER[codeDigits];
		if (addChecksum) {
			otp =  (otp * 10) + Common.calcChecksum(otp, codeDigits);
		}
		result = Integer.toString(otp);
		while (result.length() < digits) {
			result = "0" + result;
		}
		return result;
	}
	
	/*
	 * test method
	 */
	public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException {
		String key = "3132333435363738393031323334353637383930"; // hex
		
		System.out.println("Count  HOTP");
		for (int i=0; i<=9; i++) {
			System.out.println(i + "      " + HOTP.generateOTP(key, 6, false, i, -1));
		}
	}
}
